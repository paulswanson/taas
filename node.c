/*
 * Copyright (c) 2020, Paul Swanson, Matthew Squires and Mark Ma
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Paul Swanson, Matthew Squires and Mark Ma.
 * 4. Neither the name of the Paul Swanson, Matthew Squires and Mark Ma nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Paul Swanson, Matthew Squires and Mark Ma ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Paul Swanson, Matthew Squires and Mark Ma BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>

#include "node.h"
#include "common.h"
#include "dummy.h"
#include "server.h"

/*
 * Hard coded MAC keys for nodes
 */
uint8_t fake_mac_keys[NODES] = {3,3,3};

/*
 * Generates a unique mac key on each call
 */
uint8_t gen_mac_key()
{
        /*
         * TODO Presently, it uses mac_key array of predefined values
         * REMEMBER: fake_mac_keys needs to be NODES in length!!!
         */

        printf("gen_mac_key()\n");

        static uint8_t counter;

        return (counter < NODES) ? fake_mac_keys[counter++] : 0;
}

/*
 * Transform MAC
 * 
 * Decompose the mac key into one fragment for each server
 * Returns 0 for success, else is a fail
 *
 */
int transform_mac(struct node_state *ns)
{
        // TODO replace with a secure sharing algorithm
        printf("transform_mac()\n");
        
        if (timer_start()) {
                printf("timer start failed!\n");
                return 1;
        }

        if ((ns->mac_key < SERVERS) || (ns->mac_key <= 0)) {
                printf("mac_key too small\n");
                return 1;
        }
     
        uint8_t server_fragments[SERVERS];

        if (dummy_share(ns->mac_key, SERVERS, server_fragments)) {
                printf("couldn't share mac key!\n");
                return 1;
        }

        for (int s = 0; s < SERVERS; s++) {
                servers[s].alpha += server_fragments[s];
        }

        ns->mt_transform_mac = timer_stop();

        printf("Transformed Mac Key is: ");
        for (int s = 0; s < SERVERS; s++) {
                printf("%d ", server_fragments[s]);
        }
        printf("\n");

        //ns->mt_transform_mac = TIME_TRANSFORM_MAC;

        return 0;
}

/*
 * Calculate and store the aggregated, received values for all nodes
 */
void total_nodes()
{
        /* Aggregate mac shares for all computing parties */
        for (int n = 0; n < NODES; n++) {
                node_totals.mac_key += nodes[n].mac_key;
        }

        /* Aggregate triples shares for all computing parties */
        for (int n = 0; n < NODES; n++) {
                node_totals.a += nodes[n].a;
                node_totals.ca += nodes[n].ca;
                node_totals.b += nodes[n].b;
                node_totals.cb += nodes[n].cb;
                node_totals.c += nodes[n].c;
                node_totals.cc += nodes[n].cc;
        }
}

/*
 * Modulo addition
 */
uint8_t mod_add(uint8_t a, uint8_t b)
{
        return (a + b) % SIZE_OF_FIELD;
}

/*
 * Modulo multiplication
 */
uint8_t mod_mul(uint8_t a, uint8_t b)
{
        return (a * b) % SIZE_OF_FIELD;
}

/*
 * Perform an objective verification of the shared values
 * as received by the nodes.
 *
 * Return 0 if sharing of triples was successful, else 1 for fail
 *
 * In a full implementation of TaaS, nodes would engage in a secure
 * sharing protocol, such as Shamir, at which point, if successful,
 * all nodes would have common alpha and a values.
 *
 * Given that the secure sharing is not in the scope of this implementation
 * the verify() computationally and algorithmically achieve's the same
 * goal of determining whether or not a successful sharing was achieved.
 */
uint8_t verify()
{
        total_nodes();  

        uint8_t result = 0;

        result = mod_mul(a_minus_aa, alpha_minus_ba);
        result = mod_add(-result, node_totals.ca);
        result = mod_add(result, mod_mul(alpha_minus_ba , node_totals.a));
        result = mod_add(result, mod_mul(a_minus_aa , node_totals.mac_key));

        return (result == mod_mul(node_totals.a , node_totals.mac_key)) ? 0 : 1;
}
