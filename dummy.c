/*
 * Copyright (c) 2020, Paul Swanson, Matthew Squires and Mark Ma
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Paul Swanson, Matthew Squires and Mark Ma.
 * 4. Neither the name of the Paul Swanson, Matthew Squires and Mark Ma nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Paul Swanson, Matthew Squires and Mark Ma ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Paul Swanson, Matthew Squires and Mark Ma BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "dummy.h"
#include "common.h"

#define RAND_FLOOR 199

/*
 * This is NOT a random number generator
 */
uint8_t dummy_rand() {

        // TODO replace this with a proper PRG
        static uint8_t counter = RAND_FLOOR;

        if (counter++ < SIZE_OF_FIELD) {
                return counter;
        } else {
                counter = RAND_FLOOR;
                return counter;
        }
}

/*
 * Dummy Generate Shares
 *
 * Decompose a number into n parts, store in array[n]
 */
int dummy_share(uint8_t num, uint8_t n, uint8_t *array)
{
        // TODO I am deeply unsettled :D
        // if ((num < n) || (num < 0) || (n <= 0)) {
        //         return 1;
        // }
       
        uint8_t fragment = num / n;
        uint8_t remainder = num % n;

        /*
         * First array elements gets any remainder
         */
        if (remainder) {
                *array = fragment + remainder;
        } else {
                *array = fragment;
        }
        
        for (uint8_t i = 1; i < n; i++) {
               *(array + i) = fragment;
        }

        return 0;
}

/*
 * Dummy timer - not a timer
 */
unsigned int timer_start() {
        // TODO return success or fail timer start
        return 0;
}


unsigned int timer_stop() {
        // TODO return timer record value milliseconds
        return 0;
}
