/*
 * Copyright (c) 2020, Paul Swanson, Matthew Squires and Mark Ma
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Paul Swanson, Matthew Squires and Mark Ma.
 * 4. Neither the name of the Paul Swanson, Matthew Squires and Mark Ma nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Paul Swanson, Matthew Squires and Mark Ma ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Paul Swanson, Matthew Squires and Mark Ma BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SERVER_H
#define SERVER_H

#include <stdint.h>
#include "common.h"

/*
 * The master triples which are shared amongst the servers
 */
uint8_t master_triples[3][4];

/*
 * Aggregated broadcast values from lines 18-19
 */
uint8_t a_minus_aa, alpha_minus_ba;
uint8_t b_minus_bb, alpha_minus_bb;
uint8_t c_minus_cc, alpha_minus_bc;

/*
 * The Server State structure
 *
 * Contains a given server's share of the SP triple
 * and its three expendable triples (Aa,Ba,Ca etc..)
 * 
 * Contains a server's received instance of alpha
 * (aggregated mac key shares from nodes)
 *
 */
struct server_state {
        uint8_t alpha;
        uint8_t triples[3][4];

        /* Metric: Time spent transforming share */
        unsigned int mt_transform_share;
};

/*
 * Example usage of addressing triples array
 * The indexing constants are defined in common.h
 *
 * triples[A_][0];      // A
 * triples[A_][_b];     // Ab
 * triples[C_][_a];     // Ca
 *
 */

/*
 * Servers array
 *
 * Represents all servers in memory
 */
struct server_state servers[SERVERS];

/*
 * Server related function declarations
 */
int assign_server_shares(uint8_t num, uint8_t column, uint8_t row);
int transform_share(uint8_t num, uint8_t n, uint8_t *array);
#endif
