/*
 * Copyright (c) 2020, Paul Swanson, Matthew Squires and Mark Ma
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Paul Swanson, Matthew Squires and Mark Ma.
 * 4. Neither the name of the Paul Swanson, Matthew Squires and Mark Ma nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Paul Swanson, Matthew Squires and Mark Ma ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Paul Swanson, Matthew Squires and Mark Ma BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NODE_H
#define NODE_H

#include <stdint.h>
#include "common.h"

/*
 * Node State
 * 
 * Contains all state data relating to nodes
 */
struct node_state {
        uint8_t mac_key;

        /*
         * The aggregated values from each server
         * in lines 14-17
         */
        uint8_t a, b, c;
        uint8_t ca, cb, cc;
        
        /* Metric: time spent generating key */
        unsigned int mt_gen_mac_key;

        /* Metric: time spent transforming MAC */
        unsigned int mt_transform_mac;
};

/*
 * Nodes array
 *
 * Represents all nodes in memory
 */
struct node_state nodes[NODES];

/*
 * This structure contains the aggregated values, across all nodes,
 * resulting from the server to node transfers in 
 * Figure 7 lines 14-19, for the a, b and c passes.
 *
 * This data is populated by verify() and left as global state as
 * a convenience for further analysis, if so desired by the developer.
 */
struct { 
        /* TODO aggregating large keys will break this ... sort out */
        unsigned int mac_key; // TODO Ummm check size portability!!
        uint8_t a, ca ;
        uint8_t b, cb ;
        uint8_t c, cc ;
} node_totals;

/*
 * Node specific functions
 */
uint8_t gen_mac_key();
int transform_mac(struct node_state *ns);
void total_nodes();
uint8_t verify();
uint8_t mod_add(uint8_t a, uint8_t b);
uint8_t mod_mul(uint8_t a, uint8_t b);

#endif
