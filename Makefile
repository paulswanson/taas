all: clean taas 

taas: 
	gcc -g -Werror dummy.c server.c node.c main.c -o taas

run: taas
	./taas | less
clean:
	rm -rf *.o *.out taas
