# taas

A world first implementation of Triples-as-a-Service (TaaS) as proposed by Smart and Tanguy 2019, a protocol for delivering Beaver Triples to IoT nodes for the purpose of Secure Multiparty Computation (SMC). This implementation is a simple in-memory, monolithic implementation that demonstrates the functioning of the TaaS protocol that can also be build for and run on an 8-bit AVR microcontroller (Arduino). This work was completed as part of a group project for the CSC8600 course at University of Southern Queensland (USQ). The authors and copyright holders of the project and its implementation are Paul Swanson, Matthew Squires and Mark (Ke) Ma. This work was complete in June 2020 and is based on the following work:

[TaaS: Commodity MPC via Triples-as-a-Service, Smart & Tanguy 2019](https://dl.acm.org/doi/abs/10.1145/3338466.3358918)

To view our presentation and a demonstration of the code watch our video:

[Taas for IoT Demonstation and Presentation](https://www.youtube.com/watch?v=GkwUvaiCCPc)

Please note: the attribution on the uart support and Arduino Makefile, it is not our original work.