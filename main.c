/*
 * Copyright (c) 2020, Paul Swanson, Matthew Squires and Mark Ma
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the Paul Swanson, Matthew Squires and Mark Ma.
 * 4. Neither the name of the Paul Swanson, Matthew Squires and Mark Ma nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY Paul Swanson, Matthew Squires and Mark Ma ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Paul Swanson, Matthew Squires and Mark Ma BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>

#include "common.h"
#include "dummy.h"
#include "server.h"
#include "node.h"

/*
 * Uncommment below when building for Arduino
 */
//#define ARDUINO

/* When building for Arduino, support UART / USB serial for printf() */
#ifdef ARDUINO
#include "uart.h"
#endif

int main(int argc, char** argv)
{

/*
 * When building for Arduino support IO over USB serial
 */
#ifdef ARDUINO
	uart_init();
	stdout = &uart_output;
	stdin = &uart_input;
#endif
        /*
         * This is a TaaS demo which runs as a monolithic,
         * shared-memory simulation of TaaS, over a
         * finite field of 2^8 for the purposes of demonstrating
         * how TaaS works.
         *
         * The number of nodes (NODES) and servers (SERVERS) can
         * be modified in common.h, then simply rebuild using make.
         * Nodes and servers are represented in memory by structs
         * node_state and server_state accordingly, as the arrays
         * struct node_state nodes[] and struct server_state servers[]
         *
         * This code has been written so that it can be built for
         * either x86 or AVR 8-bit, so as to provide a basis for future
         * development as full implementation, if so desired.
         */
        printf("TaaS Demo!\n");

        /*
         * Generate and assign master triples
         * to be shared amongst the servers
         */
       printf("-- Master Triples --\n");
        for (int i = 0; i < 4; i++) {
                master_triples[A_][i] = dummy_rand();
                master_triples[B_][i] = dummy_rand();
                master_triples[C_][i] = \
                (master_triples[A_][i] * master_triples[B_][i]) % SIZE_OF_FIELD;

                printf("%d: A: %d B: %d C: %d\n", i,
                                master_triples[A_][i],
                                master_triples[B_][i] ,
                                master_triples[C_][i] );
        }
        
        /*
         * Share master triples with servers
         * (Figure 7, lines 2-5)
         */ 
        for (int r = 0; r < 4; r++) {
                for (int c = 0; c < 3; c++) {
                        if(assign_server_shares(master_triples[c][r], c, r)) {
                                printf("ERROR: Failed sharing master triples with servers!\n");
                                return 1;
                        }
                }
        }

        /* Display server triples */
        for (int i = 0; i < SERVERS; i++) {
                printf("\nServer %d\n", i);
                for (int r = 0; r < 4; r++) {
                        printf("A%d: %d B%d: %d C%d: %d\n",
                                r, servers[i].triples[0][r],
                                r, servers[i].triples[1][r],
                                r, servers[i].triples[2][r]);
                }
        }       
                
        /*
         * Prepare Nodes for TaaS
         * (Figure 7, lines 6-12)
         */
        for (int i = 0; i < NODES; i++) {

                printf("\n-- Node %d --\n", i);

                /* Generate MAC keys */
                
                if (timer_start()) {
                        printf("timer start failed!\n");
                        return 1;
                }
                nodes[i].mac_key = gen_mac_key();
                nodes[i].mt_gen_mac_key = timer_stop();

                // Store computational metric value
                // node[i].mt_genkey = TIME_GEN_MAC_KEY;

                if (!nodes[i].mac_key) { 
                        printf("ERROR: couldn't generate mac key\n");
                        return 1;
                }
                printf("MAC Key is %d\n", nodes[i].mac_key);

                /* Transform MAC keys */
                if (transform_mac(&nodes[i])) {
                        printf("ERROR: couldn't transform mac key\n");
                        return 1;
                }

                /*
                 * Figure 7, line 12 'point-to-point'
                 * Note: In this implementation, transform_mac() stores
                 * the server's shares of alpha in direct in servers[]
                 */
        }

        /* 
         * Transform server shares and send 'point-to-point' to nodes
         * (Figure 7, lines 14-23)
         *
         * Note: shares are merely stored in nodes[] 
         *
         * For readability, the section (lines 13-23) repeated
         * for <a>n,S <b>n,S and <c>n,S is explicitly, and copiously,
         * expressed here. Any future implementation should of
         * course handle this properly.
         */
        for (int i = 0; i < SERVERS; i++) {

                /* Temporary storage for node fragments */
                uint8_t node_fragments[NODES];
                
                /*
                 * ---- The A Pass ----
                 */

                /* Line 14 */
                if (timer_start()) {
                        printf("timer start failed!\n");
                        return 1;
                }
                if(transform_share(servers[i].triples[A_][PRIMARY], NODES, node_fragments)) {
                        printf("ERROR: couldn't share a primary for server %d!\n", i);
                        return 1;
                }
                servers[i].mt_transform_share = timer_stop();

                /* Aggregate this server's piece of A primary with each node's a value */
                for (int n = 0; n < NODES; n++) {
                        nodes[n].a += node_fragments[n];
                }

                /* Line 15 */
                if (timer_start()) {
                        printf("timer start failed!\n");
                        return 1;
                }
                if(transform_share(servers[i].triples[C_][_a], NODES, node_fragments)) {
                        printf("ERROR: couldn't share a primary for server %d!\n", i);
                        return 1;
                }
                servers[i].mt_transform_share += timer_stop();

                /* Aggregate this server's piece of Ca with each node's Ca value */
                for (int n = 0; n < NODES; n++) {
                        nodes[n].ca += node_fragments[n];
                }

                /*
                 * Lines 18-19
                 * Broadcast values
                 */
                a_minus_aa += servers[i].triples[A_][PRIMARY] - servers[i].triples[A_][_a];
                alpha_minus_ba += servers[i].alpha - servers[i].triples[B_][_a];

                /*
                 * ---- The B Pass ----
                 */

                /* Line 14 */
                if (timer_start()) {
                        printf("timer start failed!\n");
                        return 1;
                }
                if(transform_share(servers[i].triples[B_][PRIMARY], NODES, node_fragments)) {
                        printf("ERROR: couldn't share a primary for server %d!\n", i);
                        return 1;
                }
                servers[i].mt_transform_share += timer_stop();

                /* Aggregate this server's piece of B primary with each node's b value */
                for (int n = 0; n < NODES; n++) {
                        nodes[n].b += node_fragments[n];
                }

                /* Line 15 */
                if (timer_start()) {
                        printf("timer start failed!\n");
                        return 1;
                }
                if(transform_share(servers[i].triples[C_][_b], NODES, node_fragments)) {
                        printf("ERROR: couldn't share a primary for server %d!\n", i);
                        return 1;
                }
                servers[i].mt_transform_share += timer_stop();

                /* Aggregate this server's piece of Cb with each node's Cb value */
                for (int n = 0; n < NODES; n++) {
                        nodes[n].cb += node_fragments[n];
                }

                /*
                 * Lines 18-19
                 * Broadcast values
                 */
                b_minus_bb += servers[i].triples[B_][PRIMARY] - servers[i].triples[B_][_b];
                alpha_minus_bb += servers[i].alpha - servers[i].triples[B_][_b];

                /*
                 * ---- The C Pass ----
                 */

                /* Line 14 */
                if (timer_start()) {
                        printf("timer start failed!\n");
                        return 1;
                }
                if(transform_share(servers[i].triples[C_][PRIMARY], NODES, node_fragments)) {
                        printf("ERROR: couldn't share a primary for server %d!\n", i);
                        return 1;
                }
                servers[i].mt_transform_share += timer_stop();

                /* Aggregate this server's piece of C primary with each node's c value */
                for (int n = 0; n < NODES; n++) {
                        nodes[n].c += node_fragments[n];
                }

                /* Line 15 */
                if (timer_start()) {
                        printf("timer start failed!\n");
                        return 1;
                }
                if(transform_share(servers[i].triples[C_][_c], NODES, node_fragments)) {
                        printf("ERROR: couldn't share a primary for server %d!\n", i);
                        return 1;
                }
                servers[i].mt_transform_share += timer_stop();

                /* Aggregate this server's piece of Cc with each node's Cc value */
                for (int n = 0; n < NODES; n++) {
                        nodes[n].cc += node_fragments[n];
                }

                /*
                 * Lines 18-19
                 * Broadcast values
                 */
                c_minus_cc += servers[i].triples[C_][PRIMARY] - servers[i].triples[C_][_c];
                alpha_minus_bc += servers[i].alpha - servers[i].triples[B_][_c];

        }

        /*
         * Display and aggregate the node values resulting from
         * the 'point-to-point' and 'broadcast' transfers from
         * the servers in figure 7 lines 16-17 for the a, b and c passes
         */
        uint8_t total_a = 0, total_ca = 0;
        uint8_t total_b = 0, total_cb = 0;
        uint8_t total_c = 0, total_cc = 0;

        printf("\n-- Individual Node Values --\n");
        for (int n = 0; n < NODES; n++) {
                printf("\nNode %d: a = %d ca = %d\n", n, nodes[n].a, nodes[n].ca);
                total_a += nodes[n].a;
                total_ca += nodes[n].ca;

                printf("Node %d: b = %d cb = %d\n", n, nodes[n].b, nodes[n].cb);
                total_b += nodes[n].b;
                total_cb += nodes[n].cb;
                
                printf("Node %d: c = %d cc = %d\n", n, nodes[n].c, nodes[n].cc);
                total_c += nodes[n].c;
                total_cc += nodes[n].cc;
        }

        printf("\n-- Aggregated Server Point-to-Point Values --\n");
        printf("\nTotal a: %d\n", total_a);
        printf("Total ca: %d\n", total_ca);

        printf("Total b: %d\n", total_b);
        printf("Total cb: %d\n", total_cb);

        printf("Total c: %d\n", total_c);
        printf("Total cc: %d\n", total_cc);

        printf("\n-- Aggregated Server Broadcast Values --\n");
        printf("\n(a - Aa): %d\n", a_minus_aa);
        printf("(alpha - Ba): %d\n", alpha_minus_ba);

        printf("(b - Bb): %d\n", b_minus_bb);
        printf("(alpha - Bb): %d\n", alpha_minus_bb);

        printf("(c - Cc): %d\n", c_minus_cc);
        printf("(alpha - Bc): %d\n", alpha_minus_bc);

        /*
         * As per lines 22-23, verify whether or not the shares
         * have been successful.
         *
         * In a full implementation, nodes would be engaging in a secure
         * sharing mechanism, such as Shamir, by which they would do
         * undertaking this process in a 'trusted' manner, on account of
         * a secure share. This is beyond the scope of this implementation.
         * This verification process is algorithmically equivalent.
         *
         * Note: in this implementation, a generic equation, supplied by
         * the paper's author, has been used for the verification. This is
         * in part necessary because of the additive sharing used herein.
         */
        if (verify()) {
                printf("\nSUCCESS!! Sharing has been verified!\n");
        } else {
                printf("\nFAIL!!! Verification of sharing failed!\n");
        }

        return 0;
}
